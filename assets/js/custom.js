// ************************************* Main Menu *****************************************
var widthWin = $(window).width();
if(widthWin <1024){
    $('header').addClass('fixed');
}
else {
    $('header').removeClass('fixed');
}
$(window).scroll(function(){
    if ($(window).scrollTop() >50) {
        $('.main-header').addClass('hide-menu');
        if ($(window).scrollTop() >300) {
            $('.main-header').addClass('show-menu');
        }else{
            $('.main-header').removeClass('show-menu');
        }
    }else{
        $('.main-header').removeClass('hide-menu');
    }
});

$('.fix-item > img').hover(function(){
    $('.fix-item > i').style.display = "block";
});
// ************************************* main slider *******************************************
$(function() {
    $('.main-slider .owl-carousel').owlCarousel({
        loop:true,
        autoplay:true,
        // margin:10,
        rtl:true,
        nav:true,
        items:1,
        touchDrag: true,
        mouseDrag: true,
        autoplayTimeout: 8000,
        navText: [
            "<i></i><i></i>",
            "<i></i><i></i>"
        ],
    });
    var owl = $('.main-slider');
    owl.on('translate.owl.carousel', function(event) {
        $('.owl-carousel .title-slide').removeClass('animated').hide();
        $('.owl-carousel p.desc-slide').removeClass('animated').hide();
    })
    owl.on('translated.owl.carousel', function(event) {
        $('.owl-carousel .title-slide').addClass('animated fadeInUp').show();
        $('.owl-carousel p.desc-slide').addClass('animated fadeInUp').show();
    });
})
// ************************************* incrementalNumber *******************************************
$(function () {
    $('.incremental-number').countUp();
});
// ************************************* little slider *******************************************
$(function() {
    $(".little-slider__content").slick({
        dots: false,
        rtl: true,
        infinite: true,
        autoplay: false,
        speed: 800,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            { breakpoint: 1024, settings: { slidesToShow: 6, slidesToScroll: 1, infinite: true, dots: false, autoplay: true, arrows: true} },
            { breakpoint: 981, settings: { slidesToShow: 4, slidesToScroll: 1, dots: false, autoplay: true, arrows: true } },
            { breakpoint: 700, settings: { slidesToShow: 3, slidesToScroll: 1, dots: false, autoplay: true, arrows: true } },
            { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1, dots: false, autoplay: true, arrows: true } },
        ],
    });
})

// ************************************* customerSlider *******************************************
$(function() {
    $(".customer-yasna__slider").slick({
        dots: true,
        rtl: true,
        infinite: true,
        autoplay: true,
        speed: 800,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            { breakpoint: 1024, settings: { slidesToShow: 6, slidesToScroll: 1, infinite: true, dots: true, autoplay: true} },
            { breakpoint: 981, settings: { slidesToShow: 3, slidesToScroll: 1, dots: true, autoplay: true, arrows: true } },
            { breakpoint: 700, settings: { slidesToShow: 2, slidesToScroll: 1, dots: true, autoplay: true, arrows: true } },
            { breakpoint: 480, settings: { slidesToShow: 1, slidesToScroll: 1, dots: true, autoplay: true, arrows: false } },
        ],
    });
})
// ************************************* academy yesnapars slider *******************************************
$(function() {
    $('.yasna-academy__slider .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        rtl:true,
        dots: false,
        margin: 15,
        responsiveClass: true,
        responsive: {
            0: { items: 1,nav: true },
            600: { items: 1,nav: false },
            1000: { items: 3,nav: true,loop: true,margin: 30 }
        }
    });
})
// ******************************** animation scroll page **********************************
$(function() {
    AOS.init({
        easing: 'ease-in-out-sine'
    });
    setInterval(addItem, 400);
    var itemsCounter = 1;
    var container = document.getElementById('aos-demo');
    function addItem () {
        if (itemsCounter > 42) return;
        var item = document.createElement('div');
        item.classList.add('aos-item');
        item.setAttribute('data-aos', 'fade-up');
        item.innerHTML = '<div class="aos-item__inner"><h3>' + itemsCounter + '</h3></div>';
        // container.appendChild(item);
        itemsCounter++;
    }
})
// ******************************** go up button **********************************
$(document).ready(function(){
    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
//Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
});
// ******************************** tab panel **********************************
$('.nav-tabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
})
// ************************************* recently slider *******************************************
$(function() {
    $('.recent-item__slider .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        rtl:true,
        dots: false,
        margin: 15,
        nav : true,
        responsiveClass: true,
        responsive: {
            0: { items: 1,nav: true },
            600: { items: 2,nav: false },
            1000: { items: 4,nav: true,loop: true,margin: 30 }
        }
    });
})